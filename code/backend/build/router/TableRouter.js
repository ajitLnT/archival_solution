"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const TableRepository_1 = require("../DBRepository/TableRepository");
class TableRouter {
    constructor() {
        this.GetOperators = (req, res) => {
            try {
                TableRepository_1.TableRepository.getInstance().getOperators().then((data) => {
                    res.status(200).send(data);
                }).catch((err) => {
                    res.status(500).send(err);
                });
            }
            catch (error) {
                res.status(500).send(error);
            }
        };
        this.Gates = (req, res) => {
            try {
                TableRepository_1.TableRepository.getInstance().getGates().then((data) => {
                    res.status(200).send(data);
                }).catch((err) => {
                    res.status(500).send(err);
                });
            }
            catch (error) {
                res.status(500).send(error);
            }
        };
        this.router = express_1.Router();
        this.routes();
    }
    routes() {
        this.router.get('/operators', this.GetOperators);
        this.router.get('/gates', this.Gates);
    }
}
//export
const tableRoutes = new TableRouter();
tableRoutes.routes();
exports.default = tableRoutes.router;
exports.tableRoute = tableRoutes;
//# sourceMappingURL=TableRouter.js.map