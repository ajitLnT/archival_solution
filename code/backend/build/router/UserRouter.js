"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const UserRepository_1 = require("../DBRepository/UserRepository");
const User_1 = require("../models/User");
const AuthenticationService_1 = require("../services/AuthenticationService");
class UserRouter {
    constructor() {
        this.GetUsers = (req, res) => {
            try {
                UserRepository_1.UserRepository.getInstance().getUsers().then((data) => {
                    res.status(200).send(data);
                }).catch((err) => {
                    res.status(500).send(err);
                });
            }
            catch (error) {
                res.status(500).send(error);
            }
        };
        this.GetUserTables = (req, res) => {
            try {
                if (req.headers.username) {
                    UserRepository_1.UserRepository.getInstance().getUserTables(String(req.headers.username)).then((data) => {
                        res.status(200).send(data);
                    }).catch((err) => {
                        res.status(500).send(err);
                    });
                }
                else {
                    res.status(400).send('Invalid body paramters');
                }
            }
            catch (error) {
                res.status(500).send(error);
            }
        };
        this.GetUserTableColumns = (req, res) => {
            try {
                if (req.headers.username && req.headers.tablename) {
                    UserRepository_1.UserRepository.getInstance().getUserTableColumns(String(req.headers.username), String(req.headers.tablename)).then((data) => {
                        res.status(200).send(data);
                    }).catch((err) => {
                        res.status(500).send(err);
                    });
                }
                else {
                    res.status(400).send('Invalid body paramters');
                }
            }
            catch (error) {
                res.status(500).send(error);
            }
        };
        this.RegisterUser = (req, res) => {
            try {
                this.validateUserRegistration(req)
                    .then(() => {
                    let userSchema = this.createUserRegistrationPayload(req.body);
                    UserRepository_1.UserRepository.getInstance().registerUser(userSchema).then((data) => {
                        res.status(200).send(data);
                    }).catch((err) => {
                        res.status(500).send(err);
                    });
                }).catch((err) => {
                    res.status(400).send(err);
                });
            }
            catch (error) {
                res.status(500).send(error);
            }
        };
        this.login = (req, res) => {
            try {
                if (req.headers.username && req.headers.password) {
                    UserRepository_1.UserRepository.getInstance().login(String(req.headers.username), String(req.headers.password)).then((data) => {
                        let token = AuthenticationService_1.AuthenticationService.getToken(String(req.headers.username));
                        res.setHeader("token", token);
                        res.status(200).send(data);
                    }).catch((err) => {
                        res.status(500).send(err);
                    });
                }
                else {
                    res.status(400).send('Invalid body paramters');
                }
            }
            catch (error) {
                res.status(500).send(error);
            }
        };
        this.createUserRegistrationPayload = (body) => {
            let user = new User_1.User();
            user.Email = body.email;
            user.Password = body.password;
            user.FirstName = body.firstName;
            user.LastName = body.lastName;
            user.Contact = body.contactNumber;
            return user;
        };
        this.validateUserRegistration = (req) => {
            return new Promise((success, reject) => {
                if (!req.body.email) {
                    return reject("Provide user email");
                }
                if (!req.body.password) {
                    return reject("Provide user password");
                }
                if (!req.body.confirmPassword) {
                    return reject("Provide user confirm password");
                }
                if (!req.body.firstName) {
                    return reject("Provide user first name");
                }
                if (!req.body.lastName) {
                    return reject("Provide user last name");
                }
                if (!req.body.contactNumber) {
                    return reject("Provide user contact number");
                }
                if (req.body.password !== req.body.confirmPassword) {
                    return reject("Password and confirm password does not match");
                }
                return success();
            });
        };
        this.router = express_1.Router();
        this.routes();
    }
    routes() {
        this.router.get('/', this.GetUsers);
        this.router.get('/tables', this.GetUserTables);
        this.router.get('/tables/columns', this.GetUserTableColumns);
        this.router.post('/register', this.RegisterUser);
        this.router.post('/login', this.login);
    }
}
//export
const userRoutes = new UserRouter();
userRoutes.routes();
exports.default = userRoutes.router;
//# sourceMappingURL=UserRouter.js.map