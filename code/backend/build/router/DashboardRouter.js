"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const DashboardRepository_1 = require("../DBRepository/DashboardRepository");
class DashboardRouter {
    constructor() {
        this.GetDashboardDetails = (req, res) => {
            try {
                if (req.headers.infotype) {
                    let infoType = String(req.headers.infotype);
                    DashboardRepository_1.DashboardRepository.getInstance().getDashboardDetails(infoType).then((data) => {
                        res.status(200).send(data);
                    }).catch((err) => {
                        res.status(500).send(err);
                    });
                }
                else {
                    res.status(400).send('Infotype is required');
                }
            }
            catch (error) {
                res.status(500).send(error);
            }
        };
        this.GetRunnigLogs = (req, res) => {
            try {
                if (req.headers.logid) {
                    let logId = String(req.headers.logid);
                    DashboardRepository_1.DashboardRepository.getInstance().getRunningLogs(logId).then((data) => {
                        res.status(200).send(data);
                    }).catch((err) => {
                        res.status(500).send(err);
                    });
                }
                else {
                    res.status(400).send('LogID is required');
                }
            }
            catch (error) {
                res.status(500).send(error);
            }
        };
        this.RefreshDashboardDBData = (req, res) => {
            try {
                DashboardRepository_1.DashboardRepository.getInstance().refreshDashDBData().then((data) => {
                    res.status(200).send(data);
                }).catch((err) => {
                    res.status(500).send(err);
                });
            }
            catch (error) {
                res.status(500).send(error);
            }
        };
        this.routes = () => {
            this.router.get('/', this.GetDashboardDetails);
            this.router.get('/logs', this.GetRunnigLogs);
            this.router.get('/refresh', this.RefreshDashboardDBData);
        };
        this.router = express_1.Router();
        this.routes();
    }
}
const dashboardRoute = new DashboardRouter();
dashboardRoute.routes();
exports.default = dashboardRoute.router;
//# sourceMappingURL=DashboardRouter.js.map