"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const PolicyRepository_1 = require("../DBRepository/PolicyRepository");
const UserPolicy_1 = require("../models/UserPolicy");
const ArchivalPolicy_1 = require("../models/ArchivalPolicy");
class UserPolicyRouter {
    constructor() {
        this.userTablePolicyCreation = (req, res) => {
            try {
                this.validateRequest(req)
                    .then(() => {
                    let userPolicy = this.createPolicyPayload(req.body);
                    PolicyRepository_1.PolicyRepository.getInstance().createPolicy(userPolicy).then((data) => {
                        res.status(200).send(data);
                    }).catch((err) => {
                        res.status(500).send(err);
                    });
                }).catch((err) => {
                    res.status(400).send(err);
                });
            }
            catch (error) {
                res.status(500).send(error);
            }
        };
        /**
         * searchUserPolicies
         */
        this.searchUserPolicies = (req, res) => {
            try {
                if (req.headers.username) {
                    let userPolicy = new UserPolicy_1.UserPolicy();
                    userPolicy.Table_Owner = (req.headers.username) ? String(req.headers.username) : "";
                    userPolicy.Table_Name = (req.headers.tablename) ? String(req.headers.tablename) : "";
                    PolicyRepository_1.PolicyRepository.getInstance().searchPolicies(userPolicy).then((data) => {
                        res.status(200).send(data);
                    }).catch((err) => {
                        res.status(500).send(err);
                    });
                }
                else {
                    res.status(400).send('Invalid header paramters');
                }
            }
            catch (error) {
                res.status(500).send(error);
            }
        };
        this.runUserTablePolicy = (req, res) => {
            try {
                this.validateRunPolicyRequest(req)
                    .then(() => {
                    let archivalPolicy = this.createRunArchivalPolicyPayload(req.body);
                    PolicyRepository_1.PolicyRepository.getInstance().runArchivalPolicy(archivalPolicy).then((data) => {
                        res.status(200).send(data);
                    }).catch((err) => {
                        res.status(500).send(err);
                    });
                }).catch((err) => {
                    res.status(400).send(err);
                });
            }
            catch (error) {
                res.status(500).send(error);
            }
        };
        this.getPolicyDetails = (req, res) => {
            try {
                if (req.headers.username && req.headers.policyid) {
                    PolicyRepository_1.PolicyRepository.getInstance().getPolicyDetail(String(req.headers.policyid)).then((data) => {
                        res.status(200).send(data);
                    }).catch((err) => {
                        res.status(500).send(err);
                    });
                }
                else {
                    res.status(400).send('Invalid header paramters');
                }
            }
            catch (error) {
                res.status(500).send(error);
            }
        };
        this.createPolicyPayload = (body) => {
            let userPolicy = new UserPolicy_1.UserPolicy();
            userPolicy.Table_Owner = body.tableOwner;
            userPolicy.Table_Name = body.tableName;
            userPolicy.Create_Date = body.createdDate;
            userPolicy.Last_Updated_Date = body.updatedDate;
            let retentions = [];
            let retention;
            body.retentionsData.forEach((retentionData) => {
                retention = new UserPolicy_1.RetentionData();
                retention.Column_Name = retentionData.columnName;
                retention.Column_Type = retentionData.columnType;
                retention.Column_Pos = retentionData.columnPos;
                retention.Operator = retentionData.operator;
                retention.Gate = (retentionData.gate) ? retentionData.gate : "";
                retentions.push(retention);
            });
            userPolicy.Retentions_Data = retentions;
            return userPolicy;
        };
        this.createRunArchivalPolicyPayload = (body) => {
            let archivalPolicy = new ArchivalPolicy_1.ArchivalPolicy();
            archivalPolicy.P_ID = body.pId;
            archivalPolicy.Schedule_Date = body.scheduleDate;
            archivalPolicy.Schedule_Time = body.scheduleTime;
            archivalPolicy.Threshold = body.threshold;
            let runArchivalData = [];
            let archData;
            body.archivalData.forEach((archivalData) => {
                archData = new ArchivalPolicy_1.ArchivalData();
                archData.Column_Position = archivalData.columnPosition;
                archData.Column_Value = archivalData.columnValue;
                runArchivalData.push(archData);
            });
            archivalPolicy.Archival_Value = runArchivalData;
            return archivalPolicy;
        };
        this.validateRunPolicyRequest = (req) => {
            return new Promise((success, reject) => {
                if (!req.body.pId) {
                    return reject("Provide policy id");
                }
                if (!req.body.scheduleDate) {
                    return reject("Provide schedule date");
                }
                if (!req.body.scheduleTime) {
                    return reject("Provide schedule time");
                }
                if (!req.body.threshold) {
                    return reject("Provide threshold value");
                }
                let data = req.body.archivalData;
                data.forEach((archivalData) => {
                    if (!archivalData.columnPosition) {
                        return reject("Provide column position in archival data");
                    }
                    if (!archivalData.columnValue) {
                        return reject("Provide column value in archival data");
                    }
                    return success();
                });
            });
        };
        this.validateRequest = (req) => {
            return new Promise((success, reject) => {
                if (!req.body.tableOwner) {
                    return reject("Provide table owner");
                }
                if (!req.body.tableName) {
                    return reject("Provide table name");
                }
                if (!req.body.updatedDate) {
                    return reject("Provide updated date");
                }
                if (!req.body.createdDate) {
                    return reject("Provide created date");
                }
                if (!req.body.retentionsData) {
                    return reject("Provide retention policy data");
                }
                let data = req.body.retentionsData;
                data.forEach((retentionData) => {
                    if (!retentionData.columnName) {
                        return reject("Provide column name in retentions data");
                    }
                    if (!retentionData.columnType) {
                        return reject("Provide column type in retentions data");
                    }
                    if (!retentionData.operator) {
                        return reject("Provide column operator in retentions data");
                    }
                    // if(!retentionData.gate) {
                    //     return reject("Provide column gate in retentions data");
                    // }
                    return success();
                });
            });
        };
        this.router = express_1.Router();
        this.routes();
    }
    routes() {
        this.router.post('/create', this.userTablePolicyCreation);
        this.router.get('/search', this.searchUserPolicies);
        this.router.post('/run', this.runUserTablePolicy);
        this.router.get('/detail', this.getPolicyDetails);
    }
}
//export
const userPolicyRouter = new UserPolicyRouter();
userPolicyRouter.routes();
exports.default = userPolicyRouter.router;
//# sourceMappingURL=UserPolicyRouter.js.map