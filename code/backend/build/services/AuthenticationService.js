"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const utils_1 = require("../utils/utils");
const jwt = require("jsonwebtoken");
class AuthenticationService {
}
AuthenticationService.getToken = (username) => {
    let token = jwt.sign({
        username: username
    }, utils_1.Utils.TokenSecret, {
        expiresIn: "1h"
    });
    return token;
};
AuthenticationService.verifyToken = (token) => {
    try {
        jwt.verify(token, utils_1.Utils.TokenSecret);
        return true;
    }
    catch (error) {
        return false;
    }
};
exports.AuthenticationService = AuthenticationService;
//# sourceMappingURL=AuthenticationService.js.map