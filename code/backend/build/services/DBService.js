"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const server_1 = require("../server");
const utils_1 = require("../utils/utils");
class DBService {
    constructor() {
        this.ExecuteDBQueryRequest = (dbQuery) => {
            return new Promise((resolve, reject) => {
                server_1.DBConn.execute(dbQuery, (err, result) => {
                    if (err) {
                        return reject(err);
                    }
                    if (result) {
                        return resolve(utils_1.Utils.CreateJSONObject(result));
                    }
                });
            });
        };
        this.ExecuteDBProcedureRequest = (procedure, inputBody) => {
            return new Promise((resolve, reject) => {
                server_1.DBConn.execute(procedure, inputBody, (err, result) => {
                    if (err) {
                        return reject(err);
                    }
                    if (result) {
                        return resolve(utils_1.Utils.CreateJSONObject(result));
                    }
                });
            });
        };
    }
}
DBService.getInstance = () => {
    if (!DBService.instance) {
        DBService.instance = new DBService();
    }
    return DBService.instance;
};
exports.DBService = DBService;
//# sourceMappingURL=DBService.js.map