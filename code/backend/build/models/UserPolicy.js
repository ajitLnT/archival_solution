"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class UserPolicy {
    constructor() {
        this.Enable_Flag = 1;
        this.Archival_Type = 'CTAS';
        this.Schedule_Flag = 0;
        this.convertRetentionDataToConcatString = (data) => {
            let str = '';
            data.forEach((retention) => {
                str += retention.Column_Name + ",";
                str += retention.Column_Pos + ",";
                str += retention.Column_Type + ",";
                str += retention.Operator + ",";
                str += retention.Gate + ",";
                str += retention.Brac_St + ",";
                str += retention.Brac_Ed + "#";
            });
            str = str.substring(0, str.length - 1);
            return str;
        };
    }
}
exports.UserPolicy = UserPolicy;
class RetentionData {
    constructor() {
        this.Column_Name = '';
        this.Column_Pos = "1";
        this.Operator = '=';
        this.Gate = null;
        this.Brac_St = "0";
        this.Brac_Ed = "0";
    }
}
exports.RetentionData = RetentionData;
//# sourceMappingURL=UserPolicy.js.map