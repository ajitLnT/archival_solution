"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ArchivalPolicy {
    constructor() {
        this.Schedule_Time = '000000';
        this.Threshold = '3600';
        this.convertArchivalDataToConcatString = (data) => {
            let str = '';
            data.forEach((data) => {
                str += data.Column_Position + ",";
                str += data.Column_Value + "#";
            });
            str = str.substring(0, str.length - 1);
            return str;
        };
    }
}
exports.ArchivalPolicy = ArchivalPolicy;
class ArchivalData {
}
exports.ArchivalData = ArchivalData;
//# sourceMappingURL=ArchivalPolicy.js.map