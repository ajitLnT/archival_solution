"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
//import * as mongoose from 'mongoose';
const bodyParser = require("body-parser");
const cors = require("cors");
const UserRouter_1 = require("./router/UserRouter");
const TableRouter_1 = require("./router/TableRouter");
const UserPolicyRouter_1 = require("./router/UserPolicyRouter");
const DashboardRouter_1 = require("./router/DashboardRouter");
//Oracle db import
const oracledb = require("oracledb");
// server class
class Server {
    constructor() {
        this.app = express();
        this.config();
        this.routes();
    }
    config() {
        this.connProm = oracledb.getConnection({
            user: "tool_demo",
            password: "HelloWorld##1234",
            connectString: "132.145.216.78:1521/orcl_iad2sf.sub10010909451.network.oraclevcn.com"
        });
        this.connProm.then((conn) => {
            this.conn = conn;
            exports.DBConn = conn;
            console.log('Oracle connection is established');
        }).catch((err) => {
            console.error(err);
            process.env['DBConnectionError'] = err.message;
            //this.app.set('DBConnectionError', err.message);
        });
        // config
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: true }));
        this.app.use(cors());
    }
    middleware(req, res, next) {
        console.log('middleware');
        // if (process.env['DBConnectionError']) {
        //     res.status(400).send('DB connection timeout');
        // } else {
        //     if(req.url.indexOf('login') > 0 || req.url.indexOf('register') > 0) {
        //         next();
        //         return;
        //     }
        //     if(req.headers.authorization && AuthenticationService.verifyToken(req.headers.authorization)) {
        //         next();    
        //     } else {
        //         res.status(401).send('Provide valid Authorization token');
        //     }
        // }
        next();
    }
    routes() {
        let router;
        router = express.Router();
        this.app.use('/', this.middleware);
        this.app.use('/users', UserRouter_1.default);
        this.app.use('/table', TableRouter_1.default);
        this.app.use('/policy', UserPolicyRouter_1.default);
        this.app.use('/dashboard', DashboardRouter_1.default);
    }
}
const server = new Server();
exports.App = server.app;
exports.DBConn = server.conn;
//# sourceMappingURL=server.js.map