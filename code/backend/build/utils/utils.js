"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Utils {
}
Utils.TokenSecret = "DBArchivalUser";
Utils.CreateJSONObject = (result) => {
    let resultJSONArray = [];
    if (result.outBinds) {
        return result.outBinds;
    }
    else {
        result.rows.forEach((rows) => {
            var obj = {};
            var counter = 0;
            result.metaData.forEach((meta) => {
                obj[meta.name] = rows[counter];
                counter++;
            });
            resultJSONArray.push(obj);
        });
    }
    return resultJSONArray;
};
Utils.CreateArrayFromJSONObject = (jsonBody) => {
    let jsonArray = [];
    jsonBody.forEach((jsonObj) => {
        jsonArray.push([
            jsonObj.Column_Name,
            jsonObj.Column_Pos,
            jsonObj.Column_Type,
            jsonObj.Operator,
            jsonObj.Gate,
            jsonObj.Brac_St,
            jsonObj.Brac_Ed
        ]);
    });
    return jsonArray;
};
exports.Utils = Utils;
//# sourceMappingURL=utils.js.map