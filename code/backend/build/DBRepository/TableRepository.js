"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const DBService_1 = require("../services/DBService");
class TableRepository {
    constructor() {
        this.getOperators = () => {
            return DBService_1.DBService.getInstance().ExecuteDBQueryRequest("SELECT * FROM TABLE (PKG_ARCHIVAL_TOOL.F_OPERATOR_LIST('O'))");
        };
        this.getGates = () => {
            return DBService_1.DBService.getInstance().ExecuteDBQueryRequest("SELECT * FROM TABLE (PKG_ARCHIVAL_TOOL.F_OPERATOR_LIST('G'))");
        };
    }
}
TableRepository.getInstance = () => {
    if (!TableRepository.instance) {
        TableRepository.instance = new TableRepository();
    }
    return TableRepository.instance;
};
exports.TableRepository = TableRepository;
//# sourceMappingURL=TableRepository.js.map