"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const DBService_1 = require("../services/DBService");
const oracledb = require("oracledb");
class PolicyRepository {
    constructor() {
        this.createPolicy = (userPolicy) => {
            let userPolicyJSON = {
                TABLE_OWNER: { dir: oracledb.BIND_IN, type: oracledb.STRING, val: userPolicy.Table_Owner },
                TABLE_NAME: { dir: oracledb.BIND_IN, type: oracledb.STRING, val: userPolicy.Table_Name },
                ENABLE_FLAG: { dir: oracledb.BIND_IN, type: oracledb.NUMBER, val: userPolicy.Enable_Flag },
                CREATE_DATE: { dir: oracledb.BIND_IN, type: oracledb.STRING, val: userPolicy.Create_Date },
                ARCHIVAL_TYPE: { dir: oracledb.BIND_IN, type: oracledb.STRING, val: userPolicy.Archival_Type },
                SCHEDULE_FLAG: { dir: oracledb.BIND_IN, type: oracledb.NUMBER, val: userPolicy.Schedule_Flag },
                LAST_UPDATED_DATE: { dir: oracledb.BIND_IN, type: oracledb.STRING, val: userPolicy.Last_Updated_Date },
                RETENTION_DATA: { dir: oracledb.BIND_IN, type: oracledb.STRING, val: userPolicy.convertRetentionDataToConcatString(userPolicy.Retentions_Data) },
                P_ID: { dir: oracledb.BIND_OUT, type: oracledb.STRING },
                P_OUT: { dir: oracledb.BIND_OUT, type: oracledb.NUMBER }
            };
            let procedure = `
                BEGIN
                    PKG_ARCHIVAL_TOOL.P_RETENTION_POLICY_CREATE_UI(
                    :TABLE_OWNER, :TABLE_NAME, :ENABLE_FLAG, :CREATE_DATE, :ARCHIVAL_TYPE, :SCHEDULE_FLAG, 
                    :LAST_UPDATED_DATE, :RETENTION_DATA, :P_ID, :P_OUT); 
                END;`;
            return DBService_1.DBService.getInstance().ExecuteDBProcedureRequest(procedure, userPolicyJSON);
        };
        this.searchPolicies = (userPolicy) => {
            return DBService_1.DBService.getInstance().ExecuteDBQueryRequest("SELECT * FROM TABLE(PKG_ARCHIVAL_TOOL.F_RETRIEVE_POLICY('" + userPolicy.Table_Owner + "','" + userPolicy.Table_Name + "',1))");
        };
        this.runArchivalPolicy = (archivalPolicy) => {
            let archivalPolicyJSON = {
                P_ID: { dir: oracledb.BIND_IN, type: oracledb.STRING, val: archivalPolicy.P_ID },
                SCHEDULE_DATE: { dir: oracledb.BIND_IN, type: oracledb.STRING, val: archivalPolicy.Schedule_Date },
                SCHEDULE_TIME: { dir: oracledb.BIND_IN, type: oracledb.NUMBER, val: Number(archivalPolicy.Schedule_Time) },
                THRESHOLD: { dir: oracledb.BIND_IN, type: oracledb.NUMBER, val: Number(archivalPolicy.Threshold) },
                ARCHIVAL_VALUE: { dir: oracledb.BIND_IN, type: oracledb.STRING, val: archivalPolicy.convertArchivalDataToConcatString(archivalPolicy.Archival_Value) },
                P_RUN_ID: { dir: oracledb.BIND_OUT, type: oracledb.STRING },
                P_OUT: { dir: oracledb.BIND_OUT, type: oracledb.NUMBER }
            };
            let procedure = `
                BEGIN
                    PKG_ARCHIVAL_TOOL.P_RUN_CREATE_UI(
                        :P_ID, :SCHEDULE_DATE, :SCHEDULE_TIME, :THRESHOLD, :ARCHIVAL_VALUE, :P_RUN_ID, :P_OUT
                    );
                END;`;
            return DBService_1.DBService.getInstance().ExecuteDBProcedureRequest(procedure, archivalPolicyJSON);
        };
        this.getPolicyDetail = (policyId) => {
            return DBService_1.DBService.getInstance().ExecuteDBQueryRequest("SELECT * FROM TABLE(PKG_ARCHIVAL_TOOL.F_RETRIEVE_POL_DATA('" + policyId + "'))");
        };
    }
}
PolicyRepository.getInstance = () => {
    if (!PolicyRepository.instance) {
        PolicyRepository.instance = new PolicyRepository();
    }
    return PolicyRepository.instance;
};
exports.PolicyRepository = PolicyRepository;
//# sourceMappingURL=PolicyRepository.js.map