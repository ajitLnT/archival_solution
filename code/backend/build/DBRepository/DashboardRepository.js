"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const DBService_1 = require("../services/DBService");
const oracledb = require("oracledb");
class DashboardRepository {
    constructor() {
        this.getDashboardDetails = (type) => {
            return DBService_1.DBService.getInstance().ExecuteDBQueryRequest("SELECT * FROM TABLE(PKG_ARCHIVAL_TOOL.F_DASHBOARD_DATA('" + type + "'))");
        };
        this.getRunningLogs = (logId) => {
            return DBService_1.DBService.getInstance().ExecuteDBQueryRequest("SELECT * FROM TABLE(PKG_ARCHIVAL_TOOL.F_RUN_LOG('" + logId + "'))");
        };
        this.refreshDashDBData = () => {
            let data = {
                P_IN: { dir: oracledb.BIND_IN, type: oracledb.NUMBER, val: 1 },
                P_OUT: { dir: oracledb.BIND_OUT, type: oracledb.NUMBER }
            };
            let procedure = `
            BEGIN 
                PKG_ARCHIVAL_TOOL.P_DASHBOARD_DATA(:P_IN, :P_OUT);
            END;`;
            return DBService_1.DBService.getInstance().ExecuteDBProcedureRequest(procedure, data);
        };
    }
}
DashboardRepository.getInstance = () => {
    if (!DashboardRepository.instance) {
        DashboardRepository.instance = new DashboardRepository();
    }
    return DashboardRepository.instance;
};
exports.DashboardRepository = DashboardRepository;
//# sourceMappingURL=DashboardRepository.js.map