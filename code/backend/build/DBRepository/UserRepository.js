"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const DBService_1 = require("../services/DBService");
const oracledb = require("oracledb");
class UserRepository {
    constructor() {
        this.getUsers = () => {
            return DBService_1.DBService.getInstance().ExecuteDBQueryRequest("SELECT * FROM TABLE (PKG_ARCHIVAL_TOOL.F_RETRIEVE_USERS)");
        };
        this.getUserTables = (username) => {
            return DBService_1.DBService.getInstance().ExecuteDBQueryRequest("SELECT * FROM TABLE (PKG_ARCHIVAL_TOOL.F_RETRIEVE_TABLE('" + username + "'))");
        };
        this.getUserTableColumns = (username, tablename) => {
            return DBService_1.DBService.getInstance().ExecuteDBQueryRequest("SELECT * FROM TABLE (PKG_ARCHIVAL_TOOL.F_RETRIEVE_COLUMNS('" + username + "','" + tablename + "'))");
        };
        this.registerUser = (user) => {
            let userPolicyJSON = {
                EMAIL: { dir: oracledb.BIND_IN, type: oracledb.STRING, val: user.Email },
                PASSWORD: { dir: oracledb.BIND_IN, type: oracledb.STRING, val: user.Password },
                FIRSTNAME: { dir: oracledb.BIND_IN, type: oracledb.STRING, val: user.FirstName },
                LASTNAME: { dir: oracledb.BIND_IN, type: oracledb.STRING, val: user.LastName },
                CONTACTNUMBER: { dir: oracledb.BIND_IN, type: oracledb.STRING, val: user.Contact },
                P_OUT: { dir: oracledb.BIND_OUT, type: oracledb.NUMBER }
            };
            let procedure = `
                BEGIN
                    PKG_ARCHIVAL_TOOL.P_CREATE_USER(
                    :EMAIL, :PASSWORD, :FIRSTNAME, :LASTNAME, :CONTACTNUMBER, :P_OUT); 
                END;`;
            return DBService_1.DBService.getInstance().ExecuteDBProcedureRequest(procedure, userPolicyJSON);
        };
        this.login = (username, password) => {
            let userLoginJSON = {
                USERNAME: { dir: oracledb.BIND_IN, type: oracledb.STRING, val: username },
                PASSWORD: { dir: oracledb.BIND_IN, type: oracledb.STRING, val: password },
                P_FNAME: { dir: oracledb.BIND_OUT, type: oracledb.STRING },
                P_LNAME: { dir: oracledb.BIND_OUT, type: oracledb.STRING },
                P_OUT: { dir: oracledb.BIND_OUT, type: oracledb.NUMBER }
            };
            let procedure = `
            BEGIN
                PKG_ARCHIVAL_TOOL.P_LOGIN_AUTH(:USERNAME, :PASSWORD, :P_FNAME, :P_LNAME, :P_OUT);
            END;`;
            return DBService_1.DBService.getInstance().ExecuteDBProcedureRequest(procedure, userLoginJSON);
        };
    }
}
UserRepository.getInstance = () => {
    if (!UserRepository.instance) {
        UserRepository.instance = new UserRepository();
    }
    return UserRepository.instance;
};
exports.UserRepository = UserRepository;
//# sourceMappingURL=UserRepository.js.map