import * as express from 'express';
//import * as mongoose from 'mongoose';
import * as bodyParser from 'body-parser';
import * as compression from 'compression';
import * as cors from 'cors';
import UserRouter from './router/UserRouter';
import TableRouter from './router/TableRouter';
import UserPolicyRouter from "./router/UserPolicyRouter";
import DashboardRouter from "./router/DashboardRouter";
import { Router, Request, Response, NextFunction } from "express";

//Oracle db import
import * as oracledb from 'oracledb';
import { AuthenticationService } from './services/AuthenticationService';

// server class

class Server {

    public app: express.Application;

    //Connection Variable Declaration
    public conn: oracledb.Connection;
    public connProm: Promise<oracledb.Connection>;

    constructor() {
        this.app = express();
        this.config();  
        this.routes();
    }

    public config() {
        
        this.connProm = oracledb.getConnection({
            user: "tool_demo",
            password: "HelloWorld##1234",
            connectString: "132.145.216.78:1521/orcl_iad2sf.sub10010909451.network.oraclevcn.com"
        });
        this.connProm.then((conn) => {
            this.conn = conn;
            DBConn = conn;
            console.log('Oracle connection is established');
        }).catch((err: any) => {
            console.error(err);
            process.env['DBConnectionError'] = err.message;
            //this.app.set('DBConnectionError', err.message);
        });

        // config
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: true }));
        this.app.use(cors());
    }

    private middleware(req: Request, res: Response, next: NextFunction): void {
        console.log('middleware'); 
        // if (process.env['DBConnectionError']) {
        //     res.status(400).send('DB connection timeout');
        // } else {
        //     if(req.url.indexOf('login') > 0 || req.url.indexOf('register') > 0) {
        //         next();
        //         return;
        //     }
        //     if(req.headers.authorization && AuthenticationService.verifyToken(req.headers.authorization)) {
        //         next();    
        //     } else {
        //         res.status(401).send('Provide valid Authorization token');
        //     }
        // }
        next();
    }

    public routes(): void {
        let router: express.Router;
        router = express.Router();

        this.app.use('/', this.middleware);
        this.app.use('/users', UserRouter);
        this.app.use('/table', TableRouter);
        this.app.use('/policy', UserPolicyRouter); 
        this.app.use('/dashboard', DashboardRouter);
    }
}
const server = new Server();
export var App: express.Application = server.app;
export var DBConn: oracledb.Connection = server.conn;
