export class ArchivalPolicy {
    public P_ID: string;
    public Schedule_Date: string;
    public Schedule_Time: string = '000000';
    public Threshold: string = '3600';
    public Archival_Value: ArchivalData[];
    public Run_Id: string;

    public convertArchivalDataToConcatString = (data: ArchivalData[]): string => {
        let str: string = '';
        data.forEach((data: ArchivalData) => {
            str += data.Column_Position + ",";
            str += data.Column_Value + "#";
        });

        str = str.substring(0, str.length - 1);
        return str;
    }
}

export class ArchivalData {
    public Column_Position: string;
    public Column_Value: string;
}