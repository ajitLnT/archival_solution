export class UserPolicy {
    public Table_Owner: string;
    public Table_Name: string;
    public Retentions_Data: RetentionData[];
    public Enable_Flag: number = 1;
    public Create_Date: string;
    public Archival_Type: string = 'CTAS';
    public Schedule_Flag: number = 0;
    public Last_Updated_Date: string;

    public convertRetentionDataToConcatString = (data: RetentionData[]): string => {
        let str: string = '';
        data.forEach((retention: RetentionData) => {
            str += retention.Column_Name + ",";
            str += retention.Column_Pos + ",";
            str += retention.Column_Type + ",";
            str += retention.Operator + ",";
            str += retention.Gate + ",";
            str += retention.Brac_St + ",";
            str += retention.Brac_Ed + "#";
        });

        str = str.substring(0, str.length - 1);
        return str;
    }
}

export class RetentionData {
    public Column_Name: string = '';
    public Column_Type: any;
    public Column_Pos: string = "1";
    public Operator: string = '=';
    public Gate: string = null;
    public Brac_St: string = "0";
    public Brac_Ed: string = "0";

    [key: string]: string;
}