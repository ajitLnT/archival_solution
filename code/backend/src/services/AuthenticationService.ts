import { Utils } from "../utils/utils";
import * as jwt from "jsonwebtoken";

export class AuthenticationService {
    
    public static getToken = (username: string) : string => {
        let token: string = jwt.sign( 
            { 
                username: username
            }, 
            Utils.TokenSecret, 
            { 
                expiresIn: "1h" 
            });
        return token;
    }

    public static verifyToken = (token: string) : boolean => {
        try {
            jwt.verify(token, Utils.TokenSecret);
            return true;
        } catch (error) {
            return false;
        }
    }
}