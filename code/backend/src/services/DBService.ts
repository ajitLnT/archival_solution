import { DBConn } from "../server";
import * as oracledb from 'oracledb';
import { Utils } from "../utils/utils";

export class DBService {
    
    constructor() {

    }

    private static instance: DBService;
    public static getInstance = (): DBService => {
        if(!DBService.instance) {
            DBService.instance = new DBService();
        }
        return DBService.instance;
    }

    public ExecuteDBQueryRequest = (dbQuery: string) : Promise<any> => {
        return new Promise((resolve, reject) => {
            DBConn.execute(dbQuery, (err: oracledb.DBError, result: oracledb.Result) => {
                if(err) {
                    return reject(err);
                }
                if(result) {
                    return resolve(Utils.CreateJSONObject(result));
                }
            });
        });
    }

    public ExecuteDBProcedureRequest = (procedure: string, inputBody: any) : Promise<any> => {
        return new Promise((resolve, reject) => {
            DBConn.execute(procedure, inputBody, (err: oracledb.DBError, result: oracledb.Result) => {
                if(err) {
                    return reject(err);
                }
                if(result) {
                    return resolve(Utils.CreateJSONObject(result));
                }
            });
        });
    }
}