import * as oracledb from "oracledb";

export class Utils {

    public static TokenSecret: string  = "DBArchivalUser";

    public static CreateJSONObject = (result: oracledb.Result): any[] | any => {
        let resultJSONArray: any[] = [];
        if (result.outBinds) {
            return result.outBinds;
        } else {
            result.rows.forEach((rows: any) => {
                var obj: any = {};
                var counter = 0;
                result.metaData.forEach((meta: any) => {
                    obj[meta.name] = rows[counter];
                    counter++;
                });
                resultJSONArray.push(obj);
            });
        }
        return resultJSONArray;
    }

    public static CreateArrayFromJSONObject = (jsonBody: any[]): any[] => {
        let jsonArray: any[] = [];
        jsonBody.forEach((jsonObj) => {
            jsonArray.push(
                [
                    jsonObj.Column_Name,
                    jsonObj.Column_Pos,
                    jsonObj.Column_Type,
                    jsonObj.Operator,
                    jsonObj.Gate,
                    jsonObj.Brac_St,
                    jsonObj.Brac_Ed
                ]);
        });
        return jsonArray;
    }
}