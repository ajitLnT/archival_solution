import { DBService } from "../services/DBService";
import { User } from "../models/User";
import * as oracledb from 'oracledb';

export class UserRepository {
    constructor() { }

    private static instance: UserRepository;
    public static getInstance = (): UserRepository => {
        if(!UserRepository.instance) {
            UserRepository.instance = new UserRepository();
        }
        return UserRepository.instance;
    }

    public getUsers = (): Promise<any> => {
        return DBService.getInstance().ExecuteDBQueryRequest("SELECT * FROM TABLE (PKG_ARCHIVAL_TOOL.F_RETRIEVE_USERS)");
    }

    public getUserTables = (username: string): Promise<any> => {
        return DBService.getInstance().ExecuteDBQueryRequest("SELECT * FROM TABLE (PKG_ARCHIVAL_TOOL.F_RETRIEVE_TABLE('" + username +"'))");
    }

    public getUserTableColumns = (username: string, tablename: string) : Promise<any> => {
        return DBService.getInstance().ExecuteDBQueryRequest("SELECT * FROM TABLE (PKG_ARCHIVAL_TOOL.F_RETRIEVE_COLUMNS('"+ username +"','" + tablename +"'))");
    }
    public registerUser = (user: User): Promise<any> => {
        let userPolicyJSON = {
            EMAIL: { dir: oracledb.BIND_IN, type: oracledb.STRING, val: user.Email},
            PASSWORD: { dir: oracledb.BIND_IN, type: oracledb.STRING, val: user.Password },
            FIRSTNAME: { dir: oracledb.BIND_IN, type: oracledb.STRING, val: user.FirstName },
            LASTNAME: { dir: oracledb.BIND_IN, type: oracledb.STRING, val: user.LastName },
            CONTACTNUMBER: { dir: oracledb.BIND_IN, type: oracledb.STRING, val: user.Contact },
            P_OUT: { dir: oracledb.BIND_OUT, type: oracledb.NUMBER }
        }

        let procedure: string = `
                BEGIN
                    PKG_ARCHIVAL_TOOL.P_CREATE_USER(
                    :EMAIL, :PASSWORD, :FIRSTNAME, :LASTNAME, :CONTACTNUMBER, :P_OUT); 
                END;`;

        return DBService.getInstance().ExecuteDBProcedureRequest(procedure, userPolicyJSON);
    }

    public login = (username: string, password: string) : Promise<any> => {
        let userLoginJSON = {
            USERNAME: { dir: oracledb.BIND_IN, type: oracledb.STRING, val: username },
            PASSWORD: { dir: oracledb.BIND_IN, type: oracledb.STRING, val: password },
            P_FNAME: { dir: oracledb.BIND_OUT, type: oracledb.STRING },
            P_LNAME: { dir: oracledb.BIND_OUT, type: oracledb.STRING },
            P_OUT: { dir: oracledb.BIND_OUT, type: oracledb.NUMBER }
        }
        let procedure: string = `
            BEGIN
                PKG_ARCHIVAL_TOOL.P_LOGIN_AUTH(:USERNAME, :PASSWORD, :P_FNAME, :P_LNAME, :P_OUT);
            END;`;
        return DBService.getInstance().ExecuteDBProcedureRequest(procedure, userLoginJSON);
    }
}