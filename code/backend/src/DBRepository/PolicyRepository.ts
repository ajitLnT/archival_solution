import { DBService } from "../services/DBService";
import { UserPolicy  } from "../models/UserPolicy";
import { ArchivalPolicy } from "../models/ArchivalPolicy";
import * as oracledb from 'oracledb';

export class PolicyRepository {
    constructor() { }

    private static instance: PolicyRepository;
    public static getInstance = (): PolicyRepository => {
        if(!PolicyRepository.instance) {
            PolicyRepository.instance = new PolicyRepository();
        }
        return PolicyRepository.instance;
    }

    public createPolicy = (userPolicy: UserPolicy): Promise<any> => {
        let userPolicyJSON = {
            TABLE_OWNER: { dir: oracledb.BIND_IN, type: oracledb.STRING, val: userPolicy.Table_Owner},
            TABLE_NAME: { dir: oracledb.BIND_IN, type: oracledb.STRING, val: userPolicy.Table_Name },
            ENABLE_FLAG: { dir: oracledb.BIND_IN, type: oracledb.NUMBER, val: userPolicy.Enable_Flag },
            CREATE_DATE: { dir: oracledb.BIND_IN, type: oracledb.STRING, val: userPolicy.Create_Date },
            ARCHIVAL_TYPE: { dir: oracledb.BIND_IN, type: oracledb.STRING, val: userPolicy.Archival_Type },
            SCHEDULE_FLAG: { dir: oracledb.BIND_IN, type: oracledb.NUMBER, val: userPolicy.Schedule_Flag },
            LAST_UPDATED_DATE: { dir: oracledb.BIND_IN, type: oracledb.STRING, val: userPolicy.Last_Updated_Date },
            RETENTION_DATA: { dir: oracledb.BIND_IN, type: oracledb.STRING, val: userPolicy.convertRetentionDataToConcatString(userPolicy.Retentions_Data)},
            P_ID: { dir: oracledb.BIND_OUT, type: oracledb.STRING },
            P_OUT: { dir: oracledb.BIND_OUT, type: oracledb.NUMBER }
        }

        let procedure: string = `
                BEGIN
                    PKG_ARCHIVAL_TOOL.P_RETENTION_POLICY_CREATE_UI(
                    :TABLE_OWNER, :TABLE_NAME, :ENABLE_FLAG, :CREATE_DATE, :ARCHIVAL_TYPE, :SCHEDULE_FLAG, 
                    :LAST_UPDATED_DATE, :RETENTION_DATA, :P_ID, :P_OUT); 
                END;`;

        return DBService.getInstance().ExecuteDBProcedureRequest(procedure, userPolicyJSON);
    }

    public searchPolicies = (userPolicy: UserPolicy): Promise<any> => {
        return DBService.getInstance().ExecuteDBQueryRequest("SELECT * FROM TABLE(PKG_ARCHIVAL_TOOL.F_RETRIEVE_POLICY('" + userPolicy.Table_Owner + "','" + userPolicy.Table_Name + "',1))");
    }

    public runArchivalPolicy = (archivalPolicy: ArchivalPolicy): Promise<any> => {
        let archivalPolicyJSON = {
            P_ID: { dir: oracledb.BIND_IN, type: oracledb.STRING, val: archivalPolicy.P_ID },
            SCHEDULE_DATE: { dir: oracledb.BIND_IN, type: oracledb.STRING, val: archivalPolicy.Schedule_Date },
            SCHEDULE_TIME: { dir: oracledb.BIND_IN, type: oracledb.NUMBER, val: Number(archivalPolicy.Schedule_Time) },
            THRESHOLD: { dir: oracledb.BIND_IN, type: oracledb.NUMBER, val: Number(archivalPolicy.Threshold) },
            ARCHIVAL_VALUE: { dir: oracledb.BIND_IN, type: oracledb.STRING, val: archivalPolicy.convertArchivalDataToConcatString(archivalPolicy.Archival_Value) },
            P_RUN_ID: { dir: oracledb.BIND_OUT, type: oracledb.STRING },
            P_OUT: { dir: oracledb.BIND_OUT, type: oracledb.NUMBER }
        }
        let procedure: string = `
                BEGIN
                    PKG_ARCHIVAL_TOOL.P_RUN_CREATE_UI(
                        :P_ID, :SCHEDULE_DATE, :SCHEDULE_TIME, :THRESHOLD, :ARCHIVAL_VALUE, :P_RUN_ID, :P_OUT
                    );
                END;`;
        
        return DBService.getInstance().ExecuteDBProcedureRequest(procedure, archivalPolicyJSON);
    }

    public getPolicyDetail = (policyId: string): Promise<any> => {
        return DBService.getInstance().ExecuteDBQueryRequest("SELECT * FROM TABLE(PKG_ARCHIVAL_TOOL.F_RETRIEVE_POL_DATA('" + policyId + "'))");
    }
}