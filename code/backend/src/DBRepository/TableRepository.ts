import { DBService } from "../services/DBService";

export class TableRepository {
    constructor() { }

    private static instance: TableRepository;
    public static getInstance = (): TableRepository => {
        if(!TableRepository.instance) {
            TableRepository.instance = new TableRepository();
        }
        return TableRepository.instance;
    }

    public getOperators = (): Promise<any> => {
        return DBService.getInstance().ExecuteDBQueryRequest("SELECT * FROM TABLE (PKG_ARCHIVAL_TOOL.F_OPERATOR_LIST('O'))");
    }

    public getGates = (): Promise<any> => {
        return DBService.getInstance().ExecuteDBQueryRequest("SELECT * FROM TABLE (PKG_ARCHIVAL_TOOL.F_OPERATOR_LIST('G'))");
    }
}