import { DBService } from "../services/DBService";
import * as oracledb from 'oracledb';

export class DashboardRepository {
    constructor() { }

    private static instance: DashboardRepository;
    public static getInstance = (): DashboardRepository => {
        if(!DashboardRepository.instance) {
            DashboardRepository.instance = new DashboardRepository();
        }
        return DashboardRepository.instance;
    }

    public getDashboardDetails = (type: string): Promise<any> => {
        return DBService.getInstance().ExecuteDBQueryRequest("SELECT * FROM TABLE(PKG_ARCHIVAL_TOOL.F_DASHBOARD_DATA('" + type + "'))");
    }

    public getRunningLogs = (logId: string): Promise<any> => {
        return DBService.getInstance().ExecuteDBQueryRequest("SELECT * FROM TABLE(PKG_ARCHIVAL_TOOL.F_RUN_LOG('" + logId + "'))");
    }

    public refreshDashDBData = (): Promise<any> => {
        let data = {
            P_IN: { dir: oracledb.BIND_IN, type: oracledb.NUMBER, val: 1},
            P_OUT: { dir: oracledb.BIND_OUT, type: oracledb.NUMBER }
        }
        let procedure: string = `
            BEGIN 
                PKG_ARCHIVAL_TOOL.P_DASHBOARD_DATA(:P_IN, :P_OUT);
            END;`;
        return DBService.getInstance().ExecuteDBProcedureRequest(procedure, data);
    }
}