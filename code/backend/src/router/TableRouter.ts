import { Router, Request, Response } from "express";
import { TableRepository } from "../DBRepository/TableRepository";

class TableRouter {
    
    router: Router;

    constructor() {
        this.router = Router();
        this.routes();
    }

    public GetOperators = (req: Request, res:Response): void => {
        try {
            TableRepository.getInstance().getOperators().then((data: any) => {
                res.status(200).send(data);
            }).catch((err: any) => {
                res.status(500).send(err);
            });
        } catch (error) {
            res.status(500).send(error);
        }
    }

    public Gates = (req: Request, res:Response): void => {
        try {
            TableRepository.getInstance().getGates().then((data: any) => {
                res.status(200).send(data);
            }).catch((err: any) => {
                res.status(500).send(err);
            });
        } catch (error) {
            res.status(500).send(error);
        }
    }

    routes() {
        this.router.get('/operators', this.GetOperators);
        this.router.get('/gates', this.Gates);
    }
    
}

//export
const tableRoutes = new TableRouter();
tableRoutes.routes();

export default tableRoutes.router;
export var tableRoute: TableRouter = tableRoutes;