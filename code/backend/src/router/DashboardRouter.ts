import { Router, Request, Response } from "express";
import { DashboardRepository } from "../DBRepository/DashboardRepository";

class DashboardRouter {

    router: Router;

    constructor() {
        this.router = Router();
        this.routes();
    }

    public GetDashboardDetails = (req: Request, res: Response): void => {
        try {
            if (req.headers.infotype) {
                let infoType: string = String(req.headers.infotype);
                DashboardRepository.getInstance().getDashboardDetails(infoType).then((data: any) => {
                    res.status(200).send(data);
                }).catch((err: any) => {
                    res.status(500).send(err);
                });
            } else {
                res.status(400).send('Infotype is required');
            }
        } catch (error) {
            res.status(500).send(error);
        }
    }

    public GetRunnigLogs = (req: Request, res: Response): void => {
        try {
            if(req.headers.logid) {
                let logId: string = String(req.headers.logid);
                DashboardRepository.getInstance().getRunningLogs(logId).then((data: any) => {
                    res.status(200).send(data);
                }).catch((err: any) => {
                    res.status(500).send(err);
                });
            } else {
                res.status(400).send('LogID is required');
            }
        } catch (error) {
            res.status(500).send(error);
        }
    }

    public RefreshDashboardDBData =(req: Request, res:Response): void => {
        try {
            DashboardRepository.getInstance().refreshDashDBData().then((data: any) => {
                res.status(200).send(data);
            }).catch((err: any) => {
                res.status(500).send(err);
            });
        } catch (error) {
            res.status(500).send(error);
        }
    }

    routes = (): void => {
        this.router.get('/', this.GetDashboardDetails);
        this.router.get('/logs', this.GetRunnigLogs);
        this.router.get('/refresh', this.RefreshDashboardDBData);
    }
}

const dashboardRoute = new DashboardRouter();
dashboardRoute.routes();

export default dashboardRoute.router;