import { Router, Request, Response } from "express";
import { PolicyRepository } from "../DBRepository/PolicyRepository";
import { UserPolicy, RetentionData } from "../models/UserPolicy";
import { ArchivalPolicy, ArchivalData  } from "../models/ArchivalPolicy";

class UserPolicyRouter {
    router: Router;

    constructor() {
        this.router = Router();
        this.routes();
    }

    public userTablePolicyCreation = (req: Request, res: Response): void => {
        try {
            this.validateRequest(req)
            .then(() => {
                let userPolicy = this.createPolicyPayload(req.body);
                PolicyRepository.getInstance().createPolicy(userPolicy).then((data: any) => {
                    res.status(200).send(data);
                }).catch((err: any) => {
                    res.status(500).send(err);
                });
            }).catch((err: any) => {
                res.status(400).send(err);
            });
        } catch (error) {
            res.status(500).send(error);
        }
    }

    /**
     * searchUserPolicies
     */
    public searchUserPolicies = (req: Request, res: Response): void => {
        try {
            if (req.headers.username) {
                let userPolicy: UserPolicy = new UserPolicy();
                userPolicy.Table_Owner = (req.headers.username) ? String(req.headers.username): "";
                userPolicy.Table_Name = (req.headers.tablename) ? String(req.headers.tablename): "";
                PolicyRepository.getInstance().searchPolicies(userPolicy).then((data: any) => {
                    res.status(200).send(data);
                }).catch((err: any) => {
                    res.status(500).send(err);
                });
            } else {
                res.status(400).send('Invalid header paramters');
            }

        } catch (error) {
            res.status(500).send(error);
        }
    }

    public runUserTablePolicy = (req: Request, res: Response): void => {
        try {
            this.validateRunPolicyRequest(req)
            .then(() => {
                let archivalPolicy = this.createRunArchivalPolicyPayload(req.body);
                PolicyRepository.getInstance().runArchivalPolicy(archivalPolicy).then((data: any) => {
                    res.status(200).send(data);
                }).catch((err: any) => {
                    res.status(500).send(err);
                });
            }).catch((err: any) => {
                res.status(400).send(err);
            });
        } catch (error) {
            res.status(500).send(error);
        }
    }

    public getPolicyDetails = (req: Request, res:Response): void => {
        try {
            if(req.headers.username && req.headers.policyid) {
                PolicyRepository.getInstance().getPolicyDetail(String(req.headers.policyid)).then((data:any) => {
                    res.status(200).send(data);
                }).catch((err: any) => {
                    res.status(500).send(err);
                });
            } else {
                res.status(400).send('Invalid header paramters');
            }
        } catch (error) {
            res.status(500).send(error);
        }
    }

    createPolicyPayload = (body: any): UserPolicy => {
        let userPolicy: UserPolicy = new UserPolicy();
        userPolicy.Table_Owner = body.tableOwner;
        userPolicy.Table_Name = body.tableName;
        userPolicy.Create_Date = body.createdDate;
        userPolicy.Last_Updated_Date = body.updatedDate;

        let retentions: RetentionData[] = [];
        let retention: RetentionData;

        body.retentionsData.forEach((retentionData: any) => {
            retention = new RetentionData();
            retention.Column_Name = retentionData.columnName;
            retention.Column_Type = retentionData.columnType;
            retention.Column_Pos = retentionData.columnPos;
            retention.Operator = retentionData.operator;
            retention.Gate = (retentionData.gate) ? retentionData.gate: "";
            retentions.push(retention);
        });

        userPolicy.Retentions_Data = retentions;

        return userPolicy;
    }

    createRunArchivalPolicyPayload = (body: any): ArchivalPolicy => {
        let archivalPolicy: ArchivalPolicy = new ArchivalPolicy();
        archivalPolicy.P_ID = body.pId;
        archivalPolicy.Schedule_Date = body.scheduleDate;
        archivalPolicy.Schedule_Time = body.scheduleTime;
        archivalPolicy.Threshold = body.threshold;

        let runArchivalData: ArchivalData[] = [];
        let archData: ArchivalData;

        body.archivalData.forEach((archivalData: any) => {
            archData = new ArchivalData();
            archData.Column_Position = archivalData.columnPosition;
            archData.Column_Value = archivalData.columnValue;
            runArchivalData.push(archData);
        });

        archivalPolicy.Archival_Value = runArchivalData;
        return archivalPolicy;
    }

    validateRunPolicyRequest = (req: Request): Promise<any> => {
        return new Promise((success, reject) => {
            if(!req.body.pId) {
                return reject("Provide policy id");
            }
            if(!req.body.scheduleDate) {
                return reject("Provide schedule date");
            }
            if(!req.body.scheduleTime) {
                return reject("Provide schedule time");
            }
            if(!req.body.threshold) {
                return reject("Provide threshold value");
            }
            let data = req.body.archivalData;
            data.forEach((archivalData: any) => {
                if(!archivalData.columnPosition) {
                    return reject("Provide column position in archival data");
                }
                if(!archivalData.columnValue) {
                    return reject("Provide column value in archival data");
                }
                return success();
            });
        });
    }

    validateRequest = (req: Request): Promise<any> => {
        return new Promise((success, reject) => {
            if(!req.body.tableOwner) {
                return reject("Provide table owner");
            }
            if(!req.body.tableName) {
                return reject("Provide table name");
            }
            if(!req.body.updatedDate) {
                return reject("Provide updated date");
            }
            if(!req.body.createdDate) {
                return reject("Provide created date");
            }
            if(!req.body.retentionsData) {
                return reject("Provide retention policy data");
            }
            
            let data = req.body.retentionsData;
            data.forEach((retentionData: any) => {
                if(!retentionData.columnName) {
                    return reject("Provide column name in retentions data");
                }
                if(!retentionData.columnType) {
                    return reject("Provide column type in retentions data");
                }
                if(!retentionData.operator) {
                    return reject("Provide column operator in retentions data");
                }
                // if(!retentionData.gate) {
                //     return reject("Provide column gate in retentions data");
                // }
                return success();
            });
        });
    }

    routes() {
        this.router.post('/create', this.userTablePolicyCreation);
        this.router.get('/search', this.searchUserPolicies);
        this.router.post('/run', this.runUserTablePolicy);
        this.router.get('/detail', this.getPolicyDetails);
    }
}

//export
const userPolicyRouter = new UserPolicyRouter();
userPolicyRouter.routes();

export default userPolicyRouter.router;