import { Router, Request, Response } from "express";
import { UserRepository } from "../DBRepository/UserRepository";
import { User } from "../models/User";
import { AuthenticationService } from "../services/AuthenticationService";

class UserRouter {
    router: Router;

    constructor() {
        this.router = Router();
        this.routes();
    }

    public GetUsers = (req: Request, res: Response): void => {
        try {
            UserRepository.getInstance().getUsers().then((data: any) => {
                res.status(200).send(data);
            }).catch((err: any) => {
                res.status(500).send(err);
            });
        } catch (error) {
            res.status(500).send(error);
        }
    }

    public GetUserTables = (req: Request, res: Response): void => {
        try {
            if (req.headers.username) {
                UserRepository.getInstance().getUserTables(String(req.headers.username)).then((data: any) => {
                    res.status(200).send(data);
                }).catch((err: any) => {
                    res.status(500).send(err);
                });
            } else {
                res.status(400).send('Invalid body paramters');
            } 
        } catch (error) {
            res.status(500).send(error);
        }
    }

    public GetUserTableColumns = (req: Request, res: Response): void => {
        try {
            if (req.headers.username && req.headers.tablename) {
                UserRepository.getInstance().getUserTableColumns(String(req.headers.username), String(req.headers.tablename)).then((data: any) => {
                    res.status(200).send(data);
                }).catch((err: any) => {
                    res.status(500).send(err);
                });
            } else {
                res.status(400).send('Invalid body paramters');
            }
        } catch (error) {
            res.status(500).send(error);
        }
    }

    public RegisterUser = (req: Request, res: Response): void => {
        try {
            this.validateUserRegistration(req)
            .then(() => {
                let userSchema = this.createUserRegistrationPayload(req.body);
                UserRepository.getInstance().registerUser(userSchema).then((data: any) => {
                    res.status(200).send(data);
                }).catch((err: any) => {
                    res.status(500).send(err);
                });
            }).catch((err: any) => {
                res.status(400).send(err);
            });
        } catch (error) {
            res.status(500).send(error);
        }
    }

    public login = (req: Request, res: Response): void => {
        try {
            if(req.headers.username && req.headers.password) {
                UserRepository.getInstance().login(String(req.headers.username), String(req.headers.password)).then((data: any) => {
                    let token = AuthenticationService.getToken(String(req.headers.username));
                    res.setHeader("token", token);
                    res.status(200).send(data);
                }).catch((err: any) => {
                    res.status(500).send(err);
                });
            } else {
                res.status(400).send('Invalid body paramters');
            }
        } catch (error) {
            res.status(500).send(error);
        }
    }

    createUserRegistrationPayload = (body: any): User => {
        let user: User = new User();
        user.Email = body.email;
        user.Password = body.password;
        user.FirstName = body.firstName;
        user.LastName = body.lastName;
        user.Contact = body.contactNumber;
        return user;
    }

    validateUserRegistration = (req: Request): Promise<any> => {
        return new Promise((success, reject) => {
            if(!req.body.email) {
                return reject("Provide user email");
            }
            if(!req.body.password) {
                return reject("Provide user password");
            }
            if(!req.body.confirmPassword) {
                return reject("Provide user confirm password");
            }
            if(!req.body.firstName) {
                return reject("Provide user first name");
            }
            if(!req.body.lastName) {
                return reject("Provide user last name");
            }
            if(!req.body.contactNumber) {
                return reject("Provide user contact number");
            }
            if(req.body.password !== req.body.confirmPassword) {
                return reject("Password and confirm password does not match");
            }
            return success();
        });
    }

    routes() {
        this.router.get('/', this.GetUsers);
        this.router.get('/tables', this.GetUserTables);
        this.router.get('/tables/columns', this.GetUserTableColumns);
        this.router.post('/register', this.RegisterUser);
        this.router.post('/login', this.login);
    }
}

//export
const userRoutes = new UserRouter();
userRoutes.routes();

export default userRoutes.router;