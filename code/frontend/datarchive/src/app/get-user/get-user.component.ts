import { Component, OnInit, Input } from '@angular/core';
import {Userdata, usertable, columnname} from '../dumpdata';
import { group } from '@angular/animations';
import { UserApiService } from '../services/user-api.service';
import { MatListOption } from '@angular/material';
import { elementAttribute } from '@angular/core/src/render3';
import { DatePipe } from '@angular/common';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef } from  '@angular/material';
import { PolicyCreatedComponent } from '../policy-created/policy-created.component';


const username = [];
const userTable = [];
const columnName = [];
const AllOperators = [];
 const  AllGates = [];
 export var retentionsData1 =[];
export var columnSelected = [];
let objectForLoop="";



@Component({
  selector: 'app-get-user',
  templateUrl: './get-user.component.html',
  styleUrls: ['./get-user.component.css']
})
export class GetUserComponent implements OnInit {
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  selectedUSER = '' ;
  selectedTABLE = '' ;
  AllOperators;
  AllGates;
  selectColumnValue;
  selectedOperator;
  selGateValue;
  selectedvaluegate;
  retentionData;
  // username = [...Userdata];
  username;
  // userTable = [...usertable];
  userTable;
  columnName;
  columnSelected1;
  x;
  y;
  yy;
index;
datePipe: DatePipe;
  selecteduser(selectedvalue: string) {
    this.selectedUSER = selectedvalue;
    console.log(this.selecteduser);

    this.userApiSrvc.getTable(this.selectedUSER).subscribe(
      data => {
        console.log("Data", data);

        this.userTable = data },
      err => console.error(err),
      () => console.log('done loading foods')
    );
  }

  selectedtable(selectedvaluee: string) {
    this.selectedTABLE = selectedvaluee;
    console.log("selectedtable", this.selectedTABLE);

    this.userApiSrvc.getColumns(this.selectedUSER, this.selectedTABLE).subscribe(
      data => {
        console.log("Data", data);

        this.columnName = data },
      err => console.error(err),
      () => console.log('done loading foods')
    );
  }
  getNowDate() {
    //return string
    var returnDate = "";
    //get datetime now
    var today = new Date();
    //split
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //because January is 0!
    var yyyy = today.getFullYear();
    //this.yy=yyyy.toString().slice(0,-2);
    //Interpolation date
    if (dd < 10) {
        returnDate += `0${dd}-`;
    } else {
        returnDate += `${dd}-`;
    }

    // if (mm < 10) {
    //    // returnDate += `0${mm}-`;
    // } else {
    //    // returnDate += `${mm}-`;
    // }
    switch(mm){
      case 1: returnDate +=`JAN-`;
          break;
      case 2: returnDate += `FEB-`;
          break;
      case 3: returnDate += `MAR-`;
          break;
      case 4: returnDate += `APR-`;
          break;
      case 5: returnDate += `MAY-`;
          break;
      case 6: returnDate += `JUN-`;
          break;
      case 7: returnDate += `JUL-`;
          break;
      case 8: returnDate += `AUG-`;
          break;
      case 9: returnDate += `SEP-`;
          break;
      case 10: returnDate += `OCT-`;
          break;
      case 11: returnDate += `NOV-`;
          break;
      case 12: returnDate += `DEC-`;
          break;
      }
    returnDate += yyyy;
    return returnDate;
}
  onGroupsChange(options: MatListOption[]) {
    // map these MatListOptions to their values
    this.columnSelected1=options.map(o => o.value);
    columnSelected=options;
    console.log(options.map(o => o.value));
    //console.log('This is map 0 --->',options[]);
    console.log('col', this.columnSelected1);
    console.log("Index is ",options.length);
    // console.log(columnSelected);
     this.y=options.length;

/*
    this.x={
      "tableOwner": '1',
      "tableName": '1',
      "retentionsData": [
       {

        "columnName": '1',
        "columnType": '1',
        "operator": '>',
        "gate": 'OR'
       },
       {

        "columnName": '1.2',
        "columnType": '1.2',
        "operator": '>',
        "gate": 'OR'
       }

      ]
   }
 this.x.retentionsData.push({

      "columnName": '1.3',
      "columnType": '1.3',
      "operator": '>',
      "gate": 'OR'
     },

    );
   // if(this.x.retentionsData[])
   console.log("This is X - 3" ,this.x);
   */
  //var date = new Date();
   //console.log(this.datePipe.transform(date,"yyyy-MM-dd"));
var d=this.getNowDate();
    options.forEach(element => {


           this.x={
        "tableOwner": this.selectedUSER,
        "tableName": this.selectedTABLE,
        "createdDate": d,
        "updatedDate" : d,
        "retentionsData": [
         {

          "columnName": '',
          "columnType": '',
          "operator": '',
          "gate": 'NULL',
         }
        ]
     }
     options.forEach(element => {
      this.x.retentionsData.push({

        "columnName": element.value.COLUMN_NAME,
        "columnType": element.value.COLUMN_TYPE,
        "operator": '',
        "gate": ''
       },

       );
    //   this.y=this.y-1;
     //  console.log("This is y" ,this.y);
      // console.log("This is Element" ,element.);
     });
   /*  */
      this.x.retentionsData.splice(0,1);
     //console.log("Y is ",this.y, 'Options [1] ',options.length);
     console.log("This is X" ,this.x);
});
  /*  this.x={
      "tableOwner": this.selectedUSER,
      "tableName": this.selectedTABLE,
      "retentionsData": [
       {

        "columnName": (options.map(o => o.value.COLUMN_NAME).splice(-index)).toString(),
        "columnType": (options.map(o => o.value.COLUMN_TYPE).splice(-index)).toString(),
        "operator": ">",
        "gate": "OR"
       }
      ]
   } */

   //this.x.push(columnName: ;
    console.log(this.x);
  }
  printValues():void
  { console.log("selectedOperator", this.selectedOperator);
    console.log("selGateValue", this.selGateValue);
  }
  selectedCondition(event,selectedvalueopr: MatListOption[]) {
    // map these MatListOptions to their values
    //this.columnSelected1=options.map(o => o.value);
    //columnSelected=options;
    //console.log(event.value);
    this.selectedOperator=event.value
    console.log('Col ->',selectedvalueopr);
    console.log('New value of selectedOperator is ', this.selectedOperator);
this.x.retentionsData.forEach(element => {

  console.log('Element in loop ->',element.columnName);
  if(element.columnName==selectedvalueopr)
  {
    console.log('Col -> matched',selectedvalueopr);
   element.operator=this.selectedOperator;
  }
});
retentionsData1=this.x.retentionsData;
this.retentionData=retentionsData1;
this.retentionData.map((obj) => {
  obj.tableOwner = this.x.tableOwner;
  obj.tableName=this.x.tableName;
  return obj;
})
console.log("New x =",this.x);
console.log("Newest retentionsData =",retentionsData1);
    // console.log(columnSelected);
  }
  selectedConditionGate(event,selectedvaluegate: MatListOption[]) {
    // map these MatListOptions to their values
    //this.columnSelected1=options.map(o => o.value);
    //columnSelected=options;
    //console.log(event.value);
    this.selGateValue=event.value
    console.log('Col ->',selectedvaluegate);
    console.log('New value of selectedvaluegate is ', this.selGateValue);
    this.x.retentionsData.forEach(element => {

      console.log('Element in loop ->',element.columnName);
      if(element.columnName==selectedvaluegate)
      {
        console.log('Col -> matched',selectedvaluegate);
       element.gate=this.selGateValue;
      }
    });
    retentionsData1=this.x.retentionsData;
    console.log("Newest x =",this.x);
    console.log("Newest retentionsData =",retentionsData1);


  }
  FinalPolicy()
  {
   // this.router.navigateByUrl('/policyCreated');
  //  this.router.navigate(['/policyCreated']);
    // console.log("FinalPolicy is Called");
    // this.userApiSrvc.createPolicy(this.selectedUSER,this.selectedTABLE,this.x).subscribe(
    //   data => {
    //     console.log("Data for Policy", data);

    //     this.yy = data },
    //   err => console.error(err),
    //   () => console.log('done loading Policy',this.yy)
    // );
   this.userApiSrvc.setX(this.x);
  }


  objectForLoop =  this.FinalPolicy();

  /*selectedCondition(selectedOperator: string) {
    //console.log("selectedCol", this.selectColumnValue);
    console.log("selectedOperator", this.selectedOperator);
  }

  selectedCondition1(selGateValue: string) {
    //console.log("selectedCol", this.selectColumnValue);
    console.log("selGateValue", this.selGateValue);
  } */
  constructor(private userApiSrvc:UserApiService,private  dialog:  MatDialog, private  router:  Router) {
    this.userApiSrvc.getUsers().subscribe(
      data => {
        console.log("Data", data);

        this.username = data },
      err => console.error(err),
      () => console.log('done loading userdata')
    );
    this.userApiSrvc.getGates().subscribe(
      data => {
        console.log("Data", data);

        this.AllGates = data },
      err => console.error(err),
      () => console.log('All Gates')
    );
    this.userApiSrvc.getOperators().subscribe(
      data => {
        console.log("Data", data);

        this.AllOperators = data },
      err => console.error(err),
      () => console.log('All Operators')
    );
  }   //getOperators




  ngOnInit() {
  }
  onNgModelChange(event){

    console.log('on ng model change', event);

  }


}

