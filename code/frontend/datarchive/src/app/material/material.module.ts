import { NgModule } from '@angular/core';
import { MatButtonModule} from '@angular/material';
import {Component} from '@angular/core';
import {MatListModule} from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatDialogModule} from '@angular/material/dialog';
const MaterialComponent = [
  MatButtonModule, MatListModule, MatIconModule, MatButtonToggleModule, MatDialogModule];

@NgModule({
  imports: [MaterialComponent, MatListModule, MatIconModule, MatButtonToggleModule, MatDialogModule],
  exports: [MaterialComponent, MatListModule, MatIconModule, MatButtonToggleModule, MatDialogModule]
})
export class MaterialModule { }
