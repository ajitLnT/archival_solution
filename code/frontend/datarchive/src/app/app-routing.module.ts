import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {GetUserComponent} from './get-user/get-user.component';
import {IndexComponent} from './index/index.component';
// import { UserRouter } from '/Users/opc/Desktop/Sol/datarchive/router/src/router/UserRouter';
import { Router, Request, Response } from "express";
import { PolicyComponent } from './policy/policy.component';
import { PolicyCreatedComponent } from './policy-created/policy-created.component';


const routes: Routes = [
  { path: 'getuser', component: GetUserComponent},
  { path: 'index', component: IndexComponent },
  {path:'policy', component:PolicyComponent},
  {path:'policyCreated', component:PolicyCreatedComponent},
  {path: '', redirectTo: '/index', pathMatch: 'full'}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
