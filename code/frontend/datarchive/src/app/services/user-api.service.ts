import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GetUserComponent } from '../get-user/get-user.component';

@Injectable({
	providedIn: 'root'
})
export class UserApiService {

  constructor(private http: HttpClient) { }
  //private mainLink : string ="http://42ea941a.ngrok.io";
	private userApiURL: string = "http://3ef44f1f.ngrok.io/users";
	// private userTableApiURL: string = `https://e8a5edb3.ngrok.io/{{users}}/tables`;
	private userTableApiURL: string = `http://3ef44f1f.ngrok.io/users/tables`;
  private userTablecolumnApiURL: string = "http://3ef44f1f.ngrok.io/users/tables/columns";
  private Operators : string = "http://3ef44f1f.ngrok.io/table/operators";
  private Gates : string = "http://3ef44f1f.ngrok.io/table/gates";
  private poilicyDetails : string ="http://3ef44f1f.ngrok.io/policy/create";
  private searchPolicy : string ="http://3ef44f1f.ngrok.io/policy/search";
  private policy_data : any;

	getUsers() {
		return this.http.get(this.userApiURL);
	}

	getTable(userName) {
		// return this.http.get(this.userTableApiURL.replace(new RegExp('{{users}}', 'gi'), userName));
		const httpOptions = {
			headers: new HttpHeaders({
				'username': userName,
			})
		};
		return this.http.get(this.userTableApiURL, httpOptions);
	}

	getColumns(userName, userTable) {
		// return this.http.get(this.userTableApiURL.replace(new RegExp('{{users}}', 'gi'), userName));
		const httpOptions = {
			headers: new HttpHeaders({
				'username': userName,
				'tablename': userTable,
			})
		};
		return this.http.get(this.userTablecolumnApiURL, httpOptions);
  }

  getOperators() {
		return this.http.get(this.Operators);
	}
  getGates() {
		return this.http.get(this.Gates);
  }

getX(){

  console.log("getter called    policy =",this.policy_data);
  return this.policy_data;
}
setX(x)
{

  console.log("setter called policy= ",this.policy_data);
  this.policy_data=x;

}
searchPolicies(userName, userTable) {
  // return this.http.get(this.userTableApiURL.replace(new RegExp('{{users}}', 'gi'), userName));
  const httpOptions = {
    headers: new HttpHeaders({
      'username': userName,
      'tablename': userTable,
    })
  };
  console.log("search with user ", userName);
  console.log("search with Table name ", userTable);
  return this.http.get(this.searchPolicy, httpOptions);
}
  createPolicy(userName,tablename,x) {
		// return this.http.get(this.userTableApiURL.replace(new RegExp('{{users}}', 'gi'), userName));
		const httpOptions = {
			headers: new HttpHeaders({
        'Content-Type':  'application/json',
				'username': userName,
				'tablename': tablename,
			})
    };
    let param=x;

    return this.http.post(this.poilicyDetails, param, httpOptions);
    // return this.http.post(this.poilicyDetails,{httpOptions , param}).subscribe(
    //   data  => {
    //   console.log("POST Request is successful ", data);
    //   },
    //   error  => {

    //   console.log("Error", error);

    //   });
    // .pipe(
    //   catchError(this.handleError('userName', userName))
    // );
		//return this.http.get(this.userTablecolumnApiURL, httpOptions);
  }
}
