import { Component, OnInit } from '@angular/core';
import { UserApiService } from '../services/user-api.service';
import { MatDialog } from '@angular/material';
import { Router } from 'express';
import {Userdata, usertable, columnname} from '../dumpdata';
import { group } from '@angular/animations';
import { MatListOption } from '@angular/material';
import { elementAttribute } from '@angular/core/src/render3';
import { DatePipe } from '@angular/common';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-policy-created',
  templateUrl: './policy-created.component.html',
  styleUrls: ['./policy-created.component.css']
})
export class PolicyCreatedComponent implements OnInit {
  yy;
  yy1;
  yy2 = [];
  selectedUSER;
  selectedTABLE;
  FinalPolicy()
  {
   // this.router.navigateByUrl('/policyCreated');
  //  this.router.navigate(['/policyCreated']);


  }
  constructor(private userApiSrvc:UserApiService) {
    console.log("FinalPolicy is Called");
    this.yy=this.userApiSrvc.getX();
    // this.userApiSrvc.getX().subscribe(
    //   data => {
    //     console.log("policy create comp", data);

    //     this.yy = data },
    //   err => console.error(err),
    //   () => console.log('done loading x for Policy',this.yy)
    // );
    console.log("x is set in service",this.yy);
    this.userApiSrvc.createPolicy(this.yy.tableOwner,this.yy.tableName,this.yy).subscribe(
      data => {
        console.log("Data for Policy", data);

        this.yy1 = data ;
      this.yy2.push(data);
    },
      err => console.error(err),
      () => console.log('done loading Policy',this.yy1 ,'This is yy2 -->',this.yy2)
    );
    for (var x in this.yy1){
       this.yy1.hasOwnProperty(x) && this.yy2.push(this.yy2[x])
    }
    console.log(this.yy2);

  }

  ngOnInit() {
  }

}
