import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PolicyCreatedComponent } from './policy-created.component';

describe('PolicyCreatedComponent', () => {
  let component: PolicyCreatedComponent;
  let fixture: ComponentFixture<PolicyCreatedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PolicyCreatedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PolicyCreatedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
