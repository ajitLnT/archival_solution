import { Component } from '@angular/core';
import {Userdata} from './dumpdata';


// const myClonedArray = [];
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'datarchive';
  todoArray = [];
  //  myClonedArray  = [...Userdata];

  addTodo(value) {
    this.todoArray.push(value);
    console.log(this.todoArray);
  }

  /*delete item*/
  deleteItem(todo) {
    for (let i = 0; i <= this.todoArray.length; i++) {
      if (todo === this.todoArray[i]) {
        this.todoArray.splice(i, 1);
      }
    }
  }

  // submit Form
  todoSubmit(value: any) {
    console.log(value);
     }
}
