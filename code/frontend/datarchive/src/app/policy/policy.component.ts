import { Component, OnInit } from '@angular/core';
import {Userdata, usertable, columnname} from '../dumpdata';
import { group } from '@angular/animations';
import { UserApiService } from '../services/user-api.service';
import { MatListOption } from '@angular/material';
import { elementAttribute } from '@angular/core/src/render3';
import { DatePipe } from '@angular/common';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef } from  '@angular/material';
import { PolicyCreatedComponent } from '../policy-created/policy-created.component';

@Component({
  selector: 'app-policy',
  templateUrl: './policy.component.html',
  styleUrls: ['./policy.component.css']
})
export class PolicyComponent implements OnInit {
  username;
policies;
selectedUSER;
selecteduser(selectedvalue: string) {
  this.selectedUSER = selectedvalue;
  console.log("Selected User ",this.selecteduser);

}
  search(term: string): void {
    if(term=='')
    {
this.policies="";
console.log('In IF');
    }
    else {
    this.userApiSrvc.searchPolicies(this.selectedUSER,term).subscribe(
      data => {
        console.log("Data", data);

        this.policies = data },
      err => console.error(err),
      () => console.log('Policies for user are ',this.policies)
    );
  }
  }
  constructor(private userApiSrvc:UserApiService) {
    this.userApiSrvc.getUsers().subscribe(
      data => {
        console.log("Data", data);

        this.username = data },
      err => console.error(err),
      () => console.log('done loading userdata')
    );
  }

  ngOnInit() {
  }

}
