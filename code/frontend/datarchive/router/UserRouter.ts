import { Router, Request, Response } from "express";

import { DBConn } from "../server";
import * as oracledb from 'oracledb';
import { stringify } from "querystring";

class UserRouter {
    router: Router;
    conn: oracledb.Connection;

    constructor() {
        this.router = Router();
        this.routes();
        if(DBConn) {
            this.conn = DBConn;
        }
    }

    public GetUsers(req: Request, res: Response): void {
        DBConn.execute("SELECT * FROM TABLE (PKG_ARCHIVAL_TOOL.F_RETRIEVE_USERS)", (err: oracledb.DBError, result: oracledb.Result) => {
            if(!err && result) {
                console.log(JSON.stringify(result.rows));
                res.send(result.rows);
            }
        })
    }

    public GetUserTables(req: Request, res: Response): void {
        //if(req.body.user) {
            DBConn.execute("SELECT * FROM TABLE (PKG_ARCHIVAL_TOOL.F_RETRIEVE_TABLE('RISHABH'))", (err: oracledb.DBError, result: oracledb.Result) => {
                if(!err && result) {
                    res.send(result.rows);
                }
            })
        //}
    }

    public GetUserTableColumns(req: Request, res: Response): void {
        //if(req.body.user && req.body.table) {
            
            DBConn.execute("SELECT * FROM TABLE (PKG_ARCHIVAL_TOOL.F_RETRIEVE_COLUMNS('RISHABH','EMP'))", (err: oracledb.DBError, result: oracledb.Result) => {
                
                if(!err && result) {
                    res.send(result.rows);
                    //res.send(this.CreateJSONObject(result));
                }
            })
        //}
    }

    public CreateJSONObject(result: oracledb.Result): any[] {
        let resultJSONArray: any[] = [];
        result.rows.forEach(function(rows:any) {
            var obj:any = {};
            var counter = 0;
            result.metaData.forEach(function(meta) {
                obj[meta.name] = rows[counter];
                counter++;
            });
            resultJSONArray.push(obj);
        });
        return resultJSONArray;
    }

    routes() {
        this.router.get('/', this.GetUsers);
        this.router.get('/tables', this.GetUserTables);
        this.router.get('/tables/columns', this.GetUserTableColumns);
    }
}

//export
const userRoutes = new UserRouter();
userRoutes.routes();

export default userRoutes.router;