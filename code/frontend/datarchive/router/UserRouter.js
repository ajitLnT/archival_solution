"use strict";
exports.__esModule = true;
var express_1 = require("express");
var server_1 = require("../server");
var UserRouter = /** @class */ (function () {
    function UserRouter() {
        this.router = express_1.Router();
        this.routes();
        if (server_1.DBConn) {
            this.conn = server_1.DBConn;
        }
    }
    UserRouter.prototype.GetUsers = function (req, res) {
        server_1.DBConn.execute("SELECT * FROM TABLE (PKG_ARCHIVAL_TOOL.F_RETRIEVE_USERS)", function (err, result) {
            if (!err && result) {
                console.log(JSON.stringify(result.rows));
                res.send(result.rows);
            }
        });
    };
    UserRouter.prototype.GetUserTables = function (req, res) {
        //if(req.body.user) {
        server_1.DBConn.execute("SELECT * FROM TABLE (PKG_ARCHIVAL_TOOL.F_RETRIEVE_TABLE('RISHABH'))", function (err, result) {
            if (!err && result) {
                res.send(result.rows);
            }
        });
        //}
    };
    UserRouter.prototype.GetUserTableColumns = function (req, res) {
        //if(req.body.user && req.body.table) {
        server_1.DBConn.execute("SELECT * FROM TABLE (PKG_ARCHIVAL_TOOL.F_RETRIEVE_COLUMNS('RISHABH','EMP'))", function (err, result) {
            if (!err && result) {
                res.send(result.rows);
                //res.send(this.CreateJSONObject(result));
            }
        });
        //}
    };
    UserRouter.prototype.CreateJSONObject = function (result) {
        var resultJSONArray = [];
        result.rows.forEach(function (rows) {
            var obj = {};
            var counter = 0;
            result.metaData.forEach(function (meta) {
                obj[meta.name] = rows[counter];
                counter++;
            });
            resultJSONArray.push(obj);
        });
        return resultJSONArray;
    };
    UserRouter.prototype.routes = function () {
        this.router.get('/', this.GetUsers);
        this.router.get('/tables', this.GetUserTables);
        this.router.get('/tables/columns', this.GetUserTableColumns);
    };
    return UserRouter;
}());
//export
var userRoutes = new UserRouter();
userRoutes.routes();
exports["default"] = userRoutes.router;
